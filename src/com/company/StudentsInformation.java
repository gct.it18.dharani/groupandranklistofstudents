package com.company;

import java.util.HashMap;

public class StudentsInformation {
    String name;
    Integer physicsMark;
    Integer chemistryMark;
    Integer mathsMark;
    Integer computerScienceMark;

    StudentsInformation() { }

    StudentsInformation(String name, Integer physicsMark, Integer chemistryMark, Integer mathsMark, Integer computerScienceMark) {
        this.name = name;
        this.physicsMark = physicsMark;
        this.chemistryMark = chemistryMark;
        this.mathsMark = mathsMark;
        this.computerScienceMark = computerScienceMark;
    }

    public HashMap<Integer, StudentsInformation> studentList() {
        HashMap<Integer, StudentsInformation> students = new HashMap<>();
        students.put(1, new StudentsInformation("Aruvi", 80, 89, 85, 72));
        students.put(2, new StudentsInformation("Bala", 78, 88, 95, 79));
        students.put(3, new StudentsInformation("Lakshmi", 89, 87, 99, 96));
        students.put(4, new StudentsInformation("Siva", 95, 88, 91, 80));
        students.put(5, new StudentsInformation("Vel", 88, 90, 96, 84));
        students.put(6, new StudentsInformation("John", 66, 55, 84, 77));
        students.put(7, new StudentsInformation("Jack", 56, 67, 81, 56));
        students.put(8, new StudentsInformation("Prasath", 66, 77, 80, 42));
        students.put(9, new StudentsInformation("Priya", 77, 87, 90, 78));
        students.put(10, new StudentsInformation("Kayal", 67, 70, 71, 68));
        return students;
    }
}
