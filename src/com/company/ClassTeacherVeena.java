package com.company;
import java.util.*;

public class ClassTeacherVeena {
    public HashMap<Integer, String> RankList(HashMap<Integer, StudentsInformation> studentList) {
        TreeMap<Double,String> rank = new TreeMap<>(Collections.reverseOrder());
        for (Map.Entry<Integer, StudentsInformation> pair : studentList.entrySet()){
            Double total = Double.valueOf(pair.getValue().physicsMark + pair.getValue().chemistryMark + pair.getValue().mathsMark +pair.getValue().computerScienceMark)/4;
            rank.put(total, pair.getValue().name);
        }
        int count = 1;
        HashMap<Integer,String> toppers = new HashMap<>();
        for (Map.Entry<Double,String> map : rank.entrySet()){
            if ( count <= 3){
                toppers.put(count++, map.getValue());
            }
            if( count > 3) {
                break;
            }
        }
        return  toppers;
    }

//        TreeMap<Double, String> myNewMap = rank.tailMap().stream()
//                .limit(3)
//                .collect(TreeMap::new, (m, e) -> m.put(e.getKey(), e.getValue()), Map::putAll);
//        List<HashMap<Double>> lastValues = Lists.newArrayList
//                (Iterables.limit(map.descendingMap().values(), 24));

    public void GroupList(HashMap<Integer, StudentsInformation> studentList) {
        ArrayList<String> computerScienceStudents = new ArrayList<>();
        ArrayList<String> biologyStudents = new ArrayList<>();
        ArrayList<String> commerceStudents = new ArrayList<>();
        ArrayList<String> unEligibleStudents = new ArrayList<>();
        for (Map.Entry<Integer, StudentsInformation> pair : studentList.entrySet()) {
            Double averageOfPCM = Double.valueOf(pair.getValue().physicsMark + pair.getValue().chemistryMark + pair.getValue().mathsMark) / 3;
            if (averageOfPCM >= 70 && pair.getValue().computerScienceMark >= 80) {
                computerScienceStudents.add(pair.getValue().name);
            } else if (averageOfPCM >= 70) {
                biologyStudents.add(pair.getValue().name);
            } else if (pair.getValue().mathsMark >= 80) {
                commerceStudents.add(pair.getValue().name);
            } else {
                unEligibleStudents.add(pair.getValue().name);
            }
        }
        System.out.println("Computer Science Students " + computerScienceStudents);
        System.out.println("Biology Students" + biologyStudents);
        System.out.println("Commerce Students " +  commerceStudents);
        System.out.println("Uneligible Students " + unEligibleStudents);
    }

    public static void main() {
        StudentsInformation studentsInfo = new StudentsInformation();
        HashMap<Integer, StudentsInformation> studentList = studentsInfo.studentList();
        ClassTeacherVeena classTeacherVeena = new ClassTeacherVeena();
        System.out.println("Class Toppers " + classTeacherVeena.RankList(studentList));
        classTeacherVeena.GroupList(studentList);
    }
}
